# colorful-world

This is a "hello-world" demo application, to create a standalone java-application for windows using `gradle`.

By standalone, it means the application is bundled with its dependencies. 
And directly it can be run on user's windows machine without installing java.

In this demo, we use a dependency library to print color-text in console.

The application will be bundled together and shipped as `colorful-world-1.0.msi`. 
A colorful world will be shown once you install the application and run the `colorful-world.exe`.

# Build

Before building the application, ensure you have installed `wixtoolset`, required by `jpackage` for windows application.

Optional, below is one way to install `wixtoolset`.

```
# Download from its github
https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311exe.zip

# Extract .zip into your Local app, e.g
~/AppData/Local/Programs/wix311

# Add PATH on your ~/.bash_profile, to make it known to jpackage
export PATH=$PATH:"/c/Users/901754/AppData/Local/Programs/wix311"
```

```
# Create a package 
./gradlew clean build jpackage --info
```

# Install

- The built package is available at `build/dist/colorful-world-1.0.msi`
- Click the installer to start the installation.
    - In the destination folder you could specify your own local folder, to exclude admin rights required, such as
      ` ~/AppData/Local/colorful-world`
- Once it is installed successfully, the program is ready to use.
- Below is how the program can be run using windows git-bash.
```bash
$ cd ~/AppData/Local/colorful-world
$ ./colorful-world.exe 
Hello colorful-world

```

